const globalFunctions = require('../../src/js/global.js')
const invoicesFuntions = require('../../src/js/invoices/index.js')
const express = require('express');
const routes = express.Router();

routes.get('/', async (req,res)=>{
    let invoices = invoicesFuntions.getInvoices()
    res.status(200).json({
        state: invoices.length > 0 ? true : false,
        invoices: invoices.sort(globalFunctions.sortArray("-emision"))
    })
})
module.exports = routes