const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const corsOptions = {
    origin: 'http://localhost:8080'
}
const app = express()
//Settings
app.set('port', process.env.PORT || 3000)

//Middlewares
app.use(morgan('dev'))
app.use(express.json())

//Static Files
app.use(express.static(__dirname + '/frontend/dist')); //Serves resources from public folder

//Routes
app.use('/api/invoices',cors(corsOptions),require('./routes/invoices/invoices'))

//Server Listenig
app.listen(app.get('port'),()=>{
    console.log("listen to port 3000")
})