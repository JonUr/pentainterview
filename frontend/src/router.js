import Vue from 'vue'
import Router from 'vue-router'
import Invoices from './views/Invoices.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'invoices',
      component: Invoices
    }
  ]
})
