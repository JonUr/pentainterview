const global = require('../global.js')

exports.getInvoices = ()=>{
    const xml2js = require('xml2js');
    const fs = require('fs');

    let Invoices = []

    global.readFolder('./data/invoicesFiles', function(filePath, stat) {
        const parser = new xml2js.Parser({ attrkey: "Invoice" });
        let xml_string = fs.readFileSync(filePath, "utf8");

        parser.parseString(xml_string, function(error, result) {
            if(error === null) {
                let dte = result.dte
                let Invoice = {}
                //Invoice Header
                Invoice.emision = dte.Invoice.emision
                Invoice.tipo = dte.Invoice.tipo
                Invoice.folio = dte.Invoice.folio
                
                //Invoice Emisor
                Invoice.emisor = {}
                Invoice.emisor.rut = dte.emisor[0].Invoice.rut
                Invoice.emisor.razonSocial = dte.emisor[0].Invoice.razonSocial

                //Invoice Receptor
                Invoice.receptor = {}
                Invoice.receptor.rut = dte.receptor[0].Invoice.rut
                Invoice.receptor.razonSocial = dte.receptor[0].Invoice.razonSocial

                //Invoice Items
                Invoice.items = []
                dte.items[0].detalle.forEach((row, key) => {
                    let item = {}
                    item.name = row._
                    item.monto = row.Invoice.monto
                    item.iva = row.Invoice.iva
                    Invoice.items.push(item)
                });
                Invoices.push(Invoice)
            }
            else {
                //console.log(error);
            }
        });
    });
    return Invoices
}